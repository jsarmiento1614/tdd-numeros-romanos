const roman = require('../romanNumerals')

test('El valor 150 recibe CL', () => {
let value = '150';
expect(roman(value)).toBe('CL');
});

test('El valor 1515 recibe MDXV', () => {
let value = '1515';
expect(roman(value)).toBe('MDXV');
});

test('El valor 551 recibe DLI', () => {
let value = '551';
expect(roman(value)).toBe('DLI');
});

test('El valor 1111 recibe MCXI', () => {
let value = '1111';
expect(roman(value)).toBe('MCXI');
});

test('El valor 1 recibe I', () => {
let value = '1';
expect(roman(value)).toBe('I');
});

test('El valor 1000 recibe M', () => {
let value = '1000';
expect(roman(value)).toBe('M');
});

test('El valor 10 recibe X', () => {
let value = '10';
expect(roman(value)).toBe('X');
});

test('El valor 15 recibe XV', () => {
let value = '15';
expect(roman(value)).toBe('XV');
});

test('El valor 110 recibe CX', () => {
let value = '110';
expect(roman(value)).toBe('CX');
});

test('El valor 115 recibe CXV', () => {
let value = '115';
expect(roman(value)).toBe('CXV');
});