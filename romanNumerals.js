// variables globales
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//        SE APLICO EN LA ARQUITECTURA DEL CODIGO  EL PRICIPIO SOLID DE RESPONSABILIDAD UNICA, DONDE CADA METODO DESARROLLADO HACE SU PROPIA TAREA.
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

var result

const inRomain = [
  { num: 1000, value: 'M' },
  { num: 500, value: 'D' },
  { num: 100, value: 'C' },
  { num: 50, value: 'L' },
  { num: 10, value: 'X' },
  { num: 5, value: 'V' },
  { num: 1, value: 'I' }
]

const romanNumerals = (numberDecimal) => {
  // VALIDAR EL PARAMETRO
  numberDecimal ? result = compareNumber(numberDecimal) : result = ' '
  return result
}

function compareNumber(value) {
  let arrayNumber = separateNumber(value);
  let numberTraslate = ''

  for (let i = 0; i < inRomain.length; i++) {
    for (let j = 0; j < arrayNumber.length; j++) {
      if (inRomain[i].num == arrayNumber[j]) {
        numberTraslate += inRomain[i].value
      }
    }
  }

  return numberTraslate
}

function separateNumber(value) {

  let arrayOfValue = []
  let zeros = ''

  for (let j = 0; j < value.length - 1; j++) {
    zeros += '0'; 
  }
  

  for (let i = 0; i < value.length; i++) {
  
    arrayOfValue.push(value[i]+zeros)
    zeros = zeros.slice(1);

  }

  return arrayOfValue

}

module.exports = romanNumerals;
